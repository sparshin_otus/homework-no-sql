﻿namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
