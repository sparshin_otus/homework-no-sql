﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDataContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;

        public MongoDataContext(IOptions<MongoDatabaseSettings> dbOptions)
        {
            var settings = dbOptions.Value;
            _client = new MongoClient(settings.ConnectionString);
            _database = _client.GetDatabase(settings.DatabaseName);
        }

        public IMongoCollection<T> Collection<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }

        public IMongoCollection<Employee> Employee => Collection<Employee>();

        public IMongoCollection<Role> Roles => Collection<Role>();

        public void DropDatabase()
        {
            var dbName = _database.DatabaseNamespace.DatabaseName;
            var isExist = IsDatabaseExistAsync().GetAwaiter().GetResult();
            if (isExist)
                _client.DropDatabase(dbName);
        }

        private async Task<bool> IsDatabaseExistAsync()
        {
            using (var cursor = await _client.ListDatabaseNamesAsync())
            {
                var dbNames = await cursor.ToListAsync();
                return dbNames.Contains(_database.DatabaseNamespace.DatabaseName);
            }
        }
    }
}
