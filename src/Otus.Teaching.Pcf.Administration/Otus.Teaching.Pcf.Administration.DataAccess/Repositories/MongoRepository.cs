﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly MongoDataContext _dataContext;
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
            _collection = _dataContext.Collection<T>();
        }

        public async Task AddAsync(T entity) => await _collection.InsertOneAsync(entity);

        public async Task DeleteAsync(T entity) => await _collection.DeleteOneAsync(e => e.Id == entity.Id);

        public async Task<IEnumerable<T>> GetAllAsync() => await _collection.Find(e => true).ToListAsync();

        public async Task<T> GetByIdAsync(Guid id) => await _collection.Find(e => e.Id == id).FirstOrDefaultAsync();

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate) => await _collection.Find(predicate).FirstOrDefaultAsync();

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids) => await _collection.Find(e => ids.Contains(e.Id)).ToListAsync();

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate) => await _collection.Find(predicate).ToListAsync();

        public async Task UpdateAsync(T entity) => await _collection.ReplaceOneAsync(e => e.Id == entity.Id, entity);
    }
}
