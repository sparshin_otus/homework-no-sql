﻿using System;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class MongoDatabaseFixture: IDisposable
    {
        private readonly MongoTestDbInitializer _mongoTestDbInitializer;
        public MongoDataContext DbContext { get; private set; }

        public MongoDatabaseFixture()
        {
            DbContext = new MongoDataContext(new MongoTestDataBaseSettings());

            _mongoTestDbInitializer = new MongoTestDbInitializer(DbContext);
            _mongoTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            DbContext.DropDatabase();
        }
    }
}