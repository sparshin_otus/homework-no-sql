﻿using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.DataAccess;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    internal class MongoTestDataBaseSettings : IOptions<MongoDatabaseSettings>
    {
        public MongoDatabaseSettings Value =>
            new MongoDatabaseSettings()
            {
                ConnectionString = "mongodb://mongoUser:mongoPass@promocode-factory-administration-db:27017/admin",
                DatabaseName = "TestDataBase"
            };
    }
}
