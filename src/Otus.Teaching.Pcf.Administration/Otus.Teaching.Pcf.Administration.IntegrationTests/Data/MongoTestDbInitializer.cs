﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class MongoTestDbInitializer
        : IDbInitializer
    {
        private readonly MongoDataContext _dataContext;

        public MongoTestDbInitializer(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.DropDatabase();
            
            _dataContext.Roles.InsertMany(TestDataFactory.Roles);
            _dataContext.Employee.InsertMany(TestDataFactory.Employees);
        }

        public void CleanDb()
        {
            _dataContext.DropDatabase();
        }
    }
}