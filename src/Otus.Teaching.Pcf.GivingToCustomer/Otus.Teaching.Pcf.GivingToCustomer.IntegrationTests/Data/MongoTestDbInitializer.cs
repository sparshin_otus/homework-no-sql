﻿using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class MongoTestDbInitializer
        : IDbInitializer
    {
        private readonly MongoDataContext _dataContext;

        public MongoTestDbInitializer(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.DropDatabase();

            _dataContext.Customers.InsertMany(TestDataFactory.Customers);
            _dataContext.Preferences.InsertMany(TestDataFactory.Preferences);
        }

        public void CleanDb()
        {
            _dataContext.DropDatabase();
        }
    }
}