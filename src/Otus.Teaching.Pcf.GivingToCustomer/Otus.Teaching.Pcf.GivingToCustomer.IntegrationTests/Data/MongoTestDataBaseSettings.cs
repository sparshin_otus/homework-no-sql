﻿using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    internal class MongoTestDataBaseSettings : IOptions<MongoDatabaseSettings>
    {
        public MongoDatabaseSettings Value =>
            new MongoDatabaseSettings()
            {
                ConnectionString = "mongodb://mongoUser:mongoPass@promocode-factory-giving-to-customer-db:27017/admin",
                DatabaseName = "TestDataBase"
            };
    }
}
