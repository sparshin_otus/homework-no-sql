﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class MongoDatabaseFixture: IDisposable
    {
        private readonly MongoTestDbInitializer _mongoTestDbInitializer;

        public MongoDataContext DbContext { get; private set; }

        public MongoDatabaseFixture()
        {
            DbContext = new MongoDataContext(new MongoTestDataBaseSettings());

            _mongoTestDbInitializer = new MongoTestDbInitializer(DbContext);
            _mongoTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _mongoTestDbInitializer.CleanDb();
        }

        
    }
}