﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
