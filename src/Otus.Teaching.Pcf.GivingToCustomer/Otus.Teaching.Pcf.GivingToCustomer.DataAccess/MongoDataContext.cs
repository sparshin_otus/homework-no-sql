﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDataContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;

        public MongoDataContext(IOptions<MongoDatabaseSettings> dbOptions)
        {
            var settings = dbOptions.Value;
            _client = new MongoClient(settings.ConnectionString);
            _database = _client.GetDatabase(settings.DatabaseName);
        }

        public IMongoCollection<T> Collection<T>(string collectionName)
        {
            return _database.GetCollection<T>(collectionName);
        }
        public IMongoCollection<T> Collection<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }

        public IMongoCollection<Customer> Customers => Collection<Customer>();

        public IMongoCollection<Preference> Preferences => Collection<Preference>();

        public IMongoCollection<PromoCode> PromoCodes => Collection<PromoCode>();


        public void DropDatabase()
        {
            var dbName = _database.DatabaseNamespace.DatabaseName;
            var isExist = IsDatabaseExistAsync().GetAwaiter().GetResult();
            if (isExist)
                _client.DropDatabase(dbName);
        }

        private async Task<bool> IsDatabaseExistAsync()
        {
            using (var cursor = await _client.ListDatabaseNamesAsync())
            {
                var dbNames = await cursor.ToListAsync();
                return dbNames.Contains(_database.DatabaseNamespace.DatabaseName);
            }
        }
    }
}
