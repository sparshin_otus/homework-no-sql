﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoDataContext _dataContext;

        public MongoDbInitializer(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.DropDatabase();

            _dataContext.Customers.InsertMany(FakeDataFactory.Customers);
            _dataContext.Preferences.InsertMany(FakeDataFactory.Preferences);
            
        }
    }
}
